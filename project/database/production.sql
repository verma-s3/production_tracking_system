-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 01, 2020 at 06:21 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `production`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `part_id` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `completion_date` date NOT NULL,
  `qty_required` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `part_id` (`part_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `part_id`, `issue_date`, `completion_date`, `qty_required`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2020-01-15', '2020-01-15', 1200, '2020-01-14 15:47:36', '2020-01-17 13:48:59', NULL),
(2, 1, '2020-01-14', '2020-01-17', 1252, '2020-01-14 15:47:36', '2020-01-17 13:48:59', NULL),
(3, 7, '2020-01-08', '2020-01-10', 1, '2020-01-21 23:51:21', '2020-01-21 23:51:21', NULL),
(4, 6, '2020-01-07', '2020-01-23', 1, '2020-01-22 00:08:49', '2020-01-22 00:08:49', NULL),
(5, 4, '2020-01-08', '2020-01-02', 23, '2020-01-27 02:15:30', '2020-02-01 18:15:02', '2020-02-01 18:15:02'),
(7, 6, '2020-02-05', '2020-02-21', 1234, '2020-02-01 18:20:47', '2020-02-01 18:20:52', '2020-02-01 18:20:52');

-- --------------------------------------------------------

--
-- Table structure for table `parts`
--

DROP TABLE IF EXISTS `parts`;
CREATE TABLE IF NOT EXISTS `parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `part_no` int(11) NOT NULL,
  `parts_desc` varchar(255) NOT NULL,
  `drag_field` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parts`
--

INSERT INTO `parts` (`id`, `part_no`, `parts_desc`, `drag_field`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, 'this is a first part', 12, '2020-01-14 12:58:10', '2020-01-15 01:53:55', NULL),
(2, 12, 'This  is a third part', 5, '2020-01-14 12:58:10', '2020-02-01 05:36:27', '2020-02-01 05:36:27'),
(6, 3, 'this is the ninth part', 85, '2020-01-16 04:21:34', '2020-01-16 04:21:34', NULL),
(4, 4, 'this is the ninth part', 4, '2020-01-15 07:54:06', '2020-02-01 18:15:02', '2020-02-01 18:15:02'),
(9, 2, '12', 1, '2020-02-01 05:36:20', '2020-02-01 05:36:20', NULL),
(7, 8, 'this is the eighth part', 45, '2020-01-22 01:00:19', '2020-01-28 04:38:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `work_orders_details`
--

DROP TABLE IF EXISTS `work_orders_details`;
CREATE TABLE IF NOT EXISTS `work_orders_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `part_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `operation_no` int(10) NOT NULL,
  `operation_desc` varchar(255) NOT NULL,
  `machine_no` varchar(100) NOT NULL,
  `dept_no` int(11) NOT NULL,
  `qty_recieved` int(11) NOT NULL,
  `qty_done` int(11) NOT NULL,
  `qty_remained` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `part_id` (`part_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_orders_details`
--

INSERT INTO `work_orders_details` (`id`, `part_id`, `order_id`, `operation_no`, `operation_desc`, `machine_no`, `dept_no`, `qty_recieved`, `qty_done`, `qty_remained`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 'this is first order with the part number one', '45', 10, 125, 10, 115, '2020-01-17 13:49:44', '2020-01-17 13:49:44', NULL),
(2, 6, 2, 2, 'this is second order with the part number tw0', '45', 10, 125, 10, 115, '2020-01-17 13:49:44', '2020-02-01 18:20:29', '2020-02-01 18:20:29'),
(3, 7, 2, 23, 'this is the new operation for the session', '34', 18, 23, 23, 23, '2020-01-29 04:00:42', '2020-01-29 04:00:42', NULL),
(4, 4, 4, 123, 'this is my new operation for editing', '34', 1, 23, 3, 20, '2020-01-31 03:22:20', '2020-02-01 18:15:02', '2020-02-01 18:15:02');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
