@extends('layout')

@section('content')
@include('partials.flash')
	<div class="row">
        <div class="container">
        	<br />
            <a href="/parts/create" class="btn btn-primary mb-1">
                Add New Part
            </a>
			<div class="col"><br />
				<table class="table">
			    	<thead class="thead-dark">
				    <tr>
				      <th scope="col">S no.</th>
				      <th scope="col">Part no.</th>
				      <th scope="col">Parts Desciption</th>
				      <th scope="col">Drag_field</th>
				      <th scope="col">Created at</th>
				      <th scope="col">Action</th>
				    </tr>
					</thead>
					<tbody>
				  	@if(count($parts)>0)
					  	@foreach($parts as $part)
					    <tr>
					      <th scope="row">{{$part->id}}</th>
					      <td>{{$part->part_no}}</td>
					      <td>{{$part->parts_desc}}</td>
					      <td>{{$part->drag_field}}</td>
					      <td>{{$part->created_at->todatestring()}}</td>
					      <td>
					      	<a href="/parts/{{$part->id}}" class="btn btn-primary btn-sm mb-1">
			                    Edit
			                </a>
			                <form class="form d-inline form-inline"action="/parts/{{$part->id}}" 
			                method="post">
			                    @csrf 
			                    @method('DELETE') 
			                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
			                </form>
					      </td>
					    </tr> 
					    @endforeach
					    @else
		                <!-- else case if no data in delivery charges list -->
		                  <tr>
		                      <td colspan="3">There is no Parts information available</td>
		                  </tr> 
				    @endif  
				    </tbody>
				</table>
			</div>
		</div>
	</div>


@endsection('content')