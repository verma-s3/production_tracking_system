@extends('layout')

@section('content')
	<div class="row">
        <div class="container">
        	<div class="card spur-card">
                <div class="card-header bg-secondary text-white">
                    <div class="spur-card-icon">
                        <i class="fas fa-chart-bar"></i>
                    </div>
                    <div class="spur-card-title"> Add New Parts Form </div>
                </div>
                <div class="card-body">
                    <form method="post" action="/parts" id="user" novalidate>
                        @csrf

                        <div class="form-group">
                            <label for="title">Part Number</label>
                            <input type="number" class="form-control" 
                                id="name" name="part_no" placeholder="Part Number" 
                                value="{{ old('part_no') }}">
                            @if($errors->has('part_no'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('part_no') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Parts Description</label>
                            <input type="text" class="form-control" 
                                id="name" name="parts_desc" placeholder="Parts Description" 
                                value="{{ old('parts_desc') }}">
                            @if($errors->has('parts_desc'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('parts_desc') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="drag_field">Drag Field</label>
                            <input type="number" class="form-control" 
                                id="drag_field" name="drag_field" placeholder="Drag Field" 
                                value="{{ old('drag_field') }}">
                            @if($errors->has('drag_field'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('drag_field') }}
                                </div>
                            @endif
                        </div>    

                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
		</div>
	</div>


@endsection('content')