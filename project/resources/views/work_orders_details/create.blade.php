@extends('layout')

@section('content')
	<div class="row">
        <div class="container">
        	<div class="card spur-card">
                <div class="card-header bg-secondary text-white">
                    <div class="spur-card-icon">
                        <i class="fas fa-chart-bar"></i>
                    </div>
                    <div class="spur-card-title"> Add New Orders Details Form </div>
                </div>
                <div class="card-body">
                    <form method="post" action="/details" id="details" novalidate>
                        @csrf

                        <div class="form-group">
                            <label for="part_id">Part Id</label>
                            <select name="part_id" class="form-control">
                                @foreach($parts as $part)
                                    <option value="{{$part->id}}">
                                        {{ $part->id }}
                                    </option>
                                @endforeach
                            </select>
                            @if($errors->has('$part->id'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('$part->id') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="order_id">Order Number</label>
                            <select name="order_id" class="form-control">
                                @foreach($orders as $order)
                                    <option value="{{$order->id}}">
                                        {{ $order->id }}
                                    </option>
                                @endforeach
                            </select>
                            @if($errors->has('id'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('id') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Operation Number</label>
                            <input type="number" class="form-control" 
                                id="name" name="operation_no" placeholder="Operation Number" 
                                value="{{ old('operation_no') }}">
                            @if($errors->has('operation_no'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('operation_no') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Operation Description</label>
                            <input type="text" class="form-control" 
                                id="name" name="operation_desc" placeholder="Operation Description" 
                                value="{{ old('operation_desc') }}">
                            @if($errors->has('operation_desc'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('operation_desc') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Machine Number</label>
                            <input type="number" class="form-control" 
                                id="name" name="machine_no" placeholder="Machine Number" 
                                value="{{ old('machine_no') }}">
                            @if($errors->has('machine_no'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('machine_no') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Department Number</label>
                            <input type="number" class="form-control" 
                                id="name" name="dept_no" placeholder="Department Number" 
                                value="{{ old('dept_no') }}">
                            @if($errors->has('dept_no'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('dept_no') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Quantity Recieved</label>
                            <input type="number" class="form-control" 
                                id="name" name="qty_recieved" placeholder="Quantity Recieved" 
                                value="{{ old('qty_recieved') }}">
                            @if($errors->has('qty_recieved'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('qty_recieved') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Quantity Done</label>
                            <input type="number" class="form-control" 
                                id="name" name="qty_done" placeholder="Quantity Done" 
                                value="{{ old('qty_done') }}">
                            @if($errors->has('qty_done'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('qty_done') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Quantity Remained</label>
                            <input type="number" class="form-control" 
                                id="name" name="qty_remained" placeholder="Quantity Remained" 
                                value="{{ old('qty_remained') }}">
                            @if($errors->has('qty_remained'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('qty_remained') }}
                                </div>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
			
		</div>
	</div>


@endsection('content')