 @extends('layout')

@section('content')
@include('partials.flash')
	<div class="row">
        <div class="container">
        	<br />
            <a href="/details/create" class="btn btn-primary mb-1">
                Add New Work order Detail
            </a>
			<div class="colclass table-responsive"><br />
				<table class="table">
			    	<thead class="thead-dark">
				    <tr>
				      <th scope="col">S no.</th>
				      <th scope="col">Operation no.</th>
				      <th scope="col">Operation Description</th>
				      <th scope="col">Machine number</th>
				      <th scope="col">Department number</th>
				      <th scope="col">Quantity Recieved</th>
				      <th scope="col">Quantity Done</th>
				      <th scope="col">Quantity Remained</th>
				      <th scope="col">Part description</th>
				      <th scope="col">Drag Field</th>
				      <th scope="col">Issue Date</th>
				      <th scope="col">Completion Date</th>
				      <th scope="col">Quantity Required</th>
				      <th scope="col">Action</th>
				    </tr>
					</thead>
					<tbody>
				  	@if(count($details)>0)
					  	@foreach($details as $detail)
					    <tr>
					      <th scope="row">{{$detail->id}}</th>
					      <td>{{$detail->operation_no}}</td>
					      <td>{{$detail->operation_desc}}</td>
					      <td>{{$detail->machine_no}}</td>
					      <td>{{$detail->dept_no}}</td>
					      <td>{{$detail->qty_recieved}}</td>
					      <td>{{$detail->qty_done}}</td>
					      <td>{{$detail->qty_remained}}</td>
					      <td>{{$detail->part->parts_desc}}</td>
					      <td>{{$detail->part->drag_field}}</td>
					      <td>{{$detail->order->issue_date}}</td>
					      <td>{{$detail->order->completion_date}}</td>
					      <td>{{$detail->order->qty_required}}</td>
					      <td>
					      	<a href="/details/{{$detail->id}}" class="btn btn-primary btn-sm mb-1">
			                    Edit
			                </a>
			                <form class="form d-inline form-inline"action="/details/{{$detail->id}}" 
			                method="post">
			                    @csrf 
			                    @method('DELETE') 
			                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
			                </form>
					      </td>
					    </tr> 
					    @endforeach
					    @else
		                <!-- else case if no data in delivery charges list -->
		                  <tr>
		                      <td colspan="3">There is no Parts information available</td>
		                  </tr> 
				    @endif  
				    </tbody>
				</table>
			</div>
		</div>
	</div>


@endsection('content')