@extends('layout')

@section('content')
	<div class="row">
        <div class="container">
        	<div class="card spur-card">
                <div class="card-header bg-secondary text-white">
                    <div class="spur-card-icon">
                        <i class="fas fa-chart-bar"></i>
                    </div>
                    <div class="spur-card-title"> Add New Orders Form </div>
                </div>
                <div class="card-body">
                    <form method="post" action="/orders" id="order" novalidate>
                        @csrf

                        <div class="form-group">
                            <label for="id">Part Number</label>
                            <select name="id" class="form-control">
                                @foreach($parts as $part)
                                    <option value="{{$part->id}}">
                                        {{ $part->id }}
                                    </option>
                                @endforeach
                            </select>
                            @if($errors->has('$part->id'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('$part->id') }}
                                </div>
                            @endif
                        </div>              

                        <div class="form-group">
                            <label for="title">Issue Date</label>
                            <input type="date" class="form-control" 
                                id="name" name="issue_date" placeholder="Issue Date" 
                                value="{{ old('issue_date') }}">
                            @if($errors->has('issue_date'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('issue_date') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Completion Date</label>
                            <input type="date" class="form-control" 
                                id="name" name="completion_date" placeholder="Completion Date" 
                                value="{{ old('completion_date') }}">
                            @if($errors->has('completion_date'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('completion_date') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Quantity Required</label>
                            <input type="number" class="form-control" 
                                id="name" name="qty_required" placeholder="Quantity Required" 
                                value="{{ old('qty_required') }}">
                            @if($errors->has('qty_required'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('qty_required') }}
                                </div>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
			
		</div>
	</div>


@endsection('content')