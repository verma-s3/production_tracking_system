@extends('layout')

@section('content')
@include('partials.flash')
	<div class="row">
        <div class="container">
        	<br />
            <a href="/orders/create" class="btn btn-primary mb-1">
                Add New Orders
            </a>
			<div class="col"><br />
				<table class="table">
			    	<thead class="thead-dark">
				    <tr>
				      <th scope="col">S no.</th>
				      <th scope="col">Part Id</th>
				      <th scope="col">Parts description</th>
				      <th scope="col">Issue Date</th>
				      <th scope="col">Completion Date</th>
				      <th scope="col">Qty Required</th>
				      <th scope="col">Action</th>
				    </tr>
					</thead>
					<tbody>

				  	@if(count($orders)>0)
					  	@foreach($orders as $order)
					    <tr>
					    
					      <th scope="row">{{$order->id}}</th>
					      <td>{{$order->part->id}}</td>
					      <td>{{$order->part->parts_desc}}</td>
					      <td>{{$order->issue_date}}</td>
					      <td>{{$order->completion_date}}</td>
					      <td>{{$order->qty_required}}</td>
					      <td>
					      	<a href="/orders/{{$order->id}}" class="btn btn-primary btn-sm mb-1">
			                    Edit
			                </a>
			                <form class="form d-inline form-inline"action="/orders/{{$order->id}}" 
			                method="post">
			                    @csrf 
			                    @method('DELETE') 
			                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
			                </form>
					      </td>
					    </tr> 
					    @endforeach
					    @else
		                <!-- else case if no data in delivery charges list -->
		                  <tr>
		                      <td colspan="3">There is no Parts information available</td>
		                  </tr> 
				    @endif  
				    </tbody>
				</table>
			</div>
		</div>
	</div>


@endsection('content')