<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Part;

class OrderController extends Controller
{
    public function index()
    {
        // getting data through model
        $orders = Order::with('part')->latest()->get();  
        // dd($orders->toArray());  
        return view('orders.index',compact('orders'));
    }

    public function create()
    {
        $parts = Part::latest()->get();
    	return view('orders.create',compact('parts'));
    }

    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $parts = Part::latest()->get();
    	return view('orders.edit',compact('order','parts'));
    }

    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);

        $status = "";
        $message = "";
        $validatedData['part_id'] = $request->id;

        if(Order::create($validatedData)){
            // $order->part()->sync(request('id'));
            // $order->save();

            $status = "success";
            $message = "Orders was successfully created!";
        } else {
            $status = "failure";
            $message = "Orders was not successfully created!";
        }

        return redirect('/orders')->with($status, $message);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);

        $status = "";
        $message = "";
        $validatedData['part_id'] = $request->id;

        if(Order::whereId($id)->update($validatedData)){
            $status = "success";
            $message = "Orders was successfully updated!";
        } else {
            $status = "failure";
            $message = "Orders was not successfully updated!";
        }

        return redirect('/orders')->with($status, $message);
    }

    public function destroy($id)
    {
        $order = Order::find($id);

        $status = "";
        $message = "";
        if($order->delete()){

            $order->detail()->delete();
            $status = "success";
            $message = "Order was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Order was not successfully deleted!";
        }
        
        return redirect('/orders')->with($status, $message);
    }

    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          "id" => 'required',
          "issue_date" => 'required',
          "completion_date" => 'required',
          "qty_required" => 'required'
        ]);

        return $validatedData;
    } 
}
