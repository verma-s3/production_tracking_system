<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Part;


class PartController extends Controller
{
    public function index()
    {
        // getting data through model
        $parts = Part::latest()->get();
        return view('parts.index',compact('parts'));
    }

    public function create()
    {
        return view('parts.create');
    }

    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);

        $status = "";
        $message = "";
        // if condition to check the error status
        if(Part::create($validatedData)){
            $status = "success";
            $message = "New Parts was successfully created!";
        } else {
            $status = "failure";
            $message = "New Parts was not successfully created!";
        }

        return redirect('/parts')->with($status, $message);   
    }

    // public function show($id)
    // {
    //     //
    // }

    
    public function edit($id)
    {
        $part = Part::findOrFail($id);
        return view('parts.edit', compact('part'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);

        $status = "";
        $message = "";
        if(Part::whereId($id)->update($validatedData)){
            $status = "success";
            $message = "Parts was successfully updated!";
        } else {
            $status = "failure";
            $message = "Parts was not successfully updated!";
        }

        return redirect('/parts')->with($status, $message);
    }

    
    public function destroy($id)
    {
        $part = Part::find($id);
        $status = "";
        $message = "";
        if($part->delete()){
            $part->order()->delete();
            $part->detail()->delete();
            $status = "success";
            $message = "Parts was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Parts was not successfully deleted!";
        }

        return redirect('/parts')->with($status, $message);
    }

    private function validateRequestData($request, $id = null)
    {
        
        $validatedData = request()->validate([
          "part_no" => 'required|numeric|max:999.99',
          "parts_desc" => 'required|string',
          "drag_field" => 'required|max:1000'
        ]);

        return $validatedData;
    }
}
