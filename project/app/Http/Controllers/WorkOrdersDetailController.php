<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\WorkOrdersDetail;
use App\Order;
use App\Part;

class WorkOrdersDetailController extends Controller
{
    public function index()
    {
        // getting data through model
        $details = WorkOrdersDetail::with('part','order')->latest()->get();
        return view('work_orders_details.index',compact('details'));
    }

    public function create()
    {
    	$parts = Part::latest()->get();
    	$orders = Order::latest()->get();
        return view('work_orders_details.create',compact('parts','orders'));
    }

    public function edit($id)
    {
    	$detail = WorkOrdersDetail::with('part','order')->findOrFail($id);
    	$parts = Part::latest()->get();
    	$orders = Order::latest()->get();
        return view('work_orders_details.edit',compact('detail','parts','orders'));
    }

    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);

        $status = "";
        $message = "";
        $validatedData['part_id'] = $request->part_id;
        $validatedData['order_id'] = $request->order_id;

            
        if(WorkOrdersDetail::create($validatedData)){
            // $order->part()->sync(request('id'));
            // $order->save();

            $status = "success";
            $message = "Orders Details was successfully created!";
        } else {
            $status = "failure";
            $message = "Orders Details was not successfully created!";
        }

        return redirect('/details')->with($status, $message);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);

        $status = "";
        $message = "";
        $validatedData['part_id'] = $request->part_id;
        $validatedData['order_id'] = $request->order_id;
        if(WorkOrdersDetail::whereId($id)->update($validatedData)){
            $status = "success";
            $message = "Orders Details was successfully updated!";
        } else {
            $status = "failure";
            $message = "Orders Details was not successfully updated!";
        }

        return redirect('/details')->with($status, $message);
    }

    public function destroy($id)
    {
        $detail = WorkOrdersDetail::find($id);

        $status = "";
        $message = "";
        if($detail->delete()){
            $status = "success";
            $message = "Orders Details was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Orders Details was not successfully deleted!";
        }
        
        return redirect('/details')->with($status, $message);
    }

    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          "part_id" => 'required',
          "order_id" => 'required',
          "operation_no" => 'required',
          "operation_desc" => 'required|string',
          "machine_no" => 'required',
          "dept_no" => 'required',
          "qty_recieved" => 'required',
          "qty_done" => 'required',
          "qty_remained" => 'required'
        ]);

        return $validatedData;
    } 
}
