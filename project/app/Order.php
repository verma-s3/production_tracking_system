<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Part;

class Order extends Model
{
    // protected $primaryKey = 'id';
    use SoftDeletes;

    protected $fillable = ['part_id','issue_date','completion_date','qty_required','created_at','updated_at'];

    public function part()
    {
        return $this->belongsTo('App\Part');
    }

    public function detail()
    {
    	return $this->hasMany('App\WorkOrdersDetail');
    }

    
}
