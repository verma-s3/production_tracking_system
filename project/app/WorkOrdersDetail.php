<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkOrdersDetail extends Model
{
    protected $primaryKey = 'id';
    use SoftDeletes;
    protected $fillable = ['part_id','order_id','operation_no','operation_desc','dept_no','machine_no','qty_recieved','qty_done','qty_remained','created_at','updated_at'];

    public function part()
    {
        return $this->belongsTo('App\Part');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
