<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Part extends Model
{
    // protected $primaryKey = 'id';
    use SoftDeletes;
    protected $fillable = ['part_no','parts_desc','drag_field','created_at','updated_at'];

    public function order()
    {
        return $this->hasMany('App\Order');
    }

    public function detail()
    {
    	return $this->hasMany('App\WorkOrdersDetail');
    }
}
