<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Routes for front pages 
Route::get('/', 'PageController@home'); 

// Route for the tables 
	Route::get('/parts', 'PartController@index');
	Route::get('/parts/create', 'PartController@create');
	Route::post('/parts', 'PartController@store');
	Route::get('/parts/{id}', 'PartController@edit');
	Route::put('/parts/{id}', 'PartController@update');
	Route::delete('/parts/{id}','PartController@destroy');
	// 
	

	Route::get('/orders','OrderController@index');
	Route::get('/orders/create','OrderController@create');
	Route::post('/orders', 'OrderController@store');
	Route::get('/orders/{id}','OrderController@edit');
	Route::put('/orders/{id}','OrderController@update');
	Route::delete('/orders/{id}','OrderController@destroy');



	Route::get('/details','WorkOrdersDetailController@index');
	Route::get('/details/create','WorkOrdersDetailController@create');
	Route::post('/details', 'WorkOrdersDetailController@store');
	Route::get('/details/{id}','WorkOrdersDetailController@edit');
	Route::put('/details/{id}','WorkOrdersDetailController@update');
	Route::delete('/details/{id}','WorkOrdersDetailController@destroy');